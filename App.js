const express = require("express")
const mongoose = require("mongoose")

const app=express()

mongoose.connect("mongodb://localhost/todo_node",{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

app.use(express.urlencoded({extended:true}));
app.use(express.static("public"));
app.set("view engine","ejs");

//routes
app.use(require("./routes/index"));
app.use(require("./routes/todo"));

//Server Configuration
app.listen(4000,()=>{
    console.log("Server started listening on port 4000")
});
